package ru.t1.godyna.tm.api.repository;

import ru.t1.godyna.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name, String description);

    Project create(String name);

    Project add(Project project);

    void clear();

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
