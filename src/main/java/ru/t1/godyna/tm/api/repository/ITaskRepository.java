package ru.t1.godyna.tm.api.repository;

import ru.t1.godyna.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task create(String name, String description);

    Task create(String name);

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task project);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
