package ru.t1.godyna.tm.api.service;

import ru.t1.godyna.tm.api.repository.ITaskRepository;
import ru.t1.godyna.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task add(Task task);

    void clear();

    Task create(String name, String description);

    Task create(String name);

    List<Task> findAll();

    void remove(Task project);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
