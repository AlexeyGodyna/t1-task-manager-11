package ru.t1.godyna.tm.api.service;

import ru.t1.godyna.tm.api.repository.IProjectRepository;
import ru.t1.godyna.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project add(Project project);

    void clear();

    Project create(String name, String description);

    Project create(String name);

    List<Project> findAll();

    void remove(Project project);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
