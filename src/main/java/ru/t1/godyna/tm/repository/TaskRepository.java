package ru.t1.godyna.tm.repository;

import ru.t1.godyna.tm.api.repository.ITaskRepository;
import ru.t1.godyna.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task create(String name, String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task create(String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task findOneById(String id) {
        for (final Task task: tasks){
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(Integer index) {
        return tasks.get(index);
    }

    @Override
    public void remove(Task task) {
        if (task == null) return;
        tasks.remove(task);
    }

    @Override
    public Task removeById(String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
